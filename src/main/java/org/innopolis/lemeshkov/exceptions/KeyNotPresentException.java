package org.innopolis.lemeshkov.exceptions;

public class KeyNotPresentException extends  Exception{
    /**
     * Create default exception
     */
    public KeyNotPresentException() {
        super();
    }

    /**
     * Create exception with message
     * @param message - message for exception
     */
    public KeyNotPresentException(String message) {
        super(message);
    }
}
