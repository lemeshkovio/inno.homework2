package org.innopolis.lemeshkov.unicomparable;


public interface UniComparable {
    /**
     * Stores a token object for comparing the null literal with other values.
     */
    UniComparableToken NULL_TOKEN = new  UniComparableToken();

    /**
     * Returns a token object that allows any objects that implement this interface to be compared
     * @return - token object
     */
    UniComparableToken getComparableToken();
}
