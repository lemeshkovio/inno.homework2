package org.innopolis.lemeshkov.unicomparable;

import java.util.Arrays;

/**
 * A token class that provides a comparison of any objects to which it is bound
 *
 */
public class UniComparableToken implements UniComparable {

    private final long[] uid = getUID();

    private static long[] nextUID = {0};
    private static int writePos = 0;

    /**
     * Assigning a serial number to a token
     * 
     * @return array which contain tokens numbers
     */
    private static long[] getUID() {
        long[] returnUID = Arrays.copyOf(nextUID, nextUID.length);
        nextUID[writePos]++;
        if (nextUID[nextUID.length - 1] == Long.MAX_VALUE) {
            writePos++;
            if (writePos == nextUID.length) {
                nextUID = new long[nextUID.length + 1];
                Arrays.fill(nextUID, 0);
                writePos = 0;
            }
        }
        return returnUID;
    }

    @Override
    public UniComparableToken getComparableToken() {
        return this;
    }

    /**
     * Compare tokens
     * 
     * @param other - current token
     * @return - true, if token less target token. False else.
     */
    public boolean greaterThan(UniComparableToken other) {
        if (this.equals(other)) {
            return false;
        }
        if (uid.length > other.uid.length) {
            return true;
        }
        if (uid.length < other.uid.length) {
            return false;
        }
        for (int i = 0; i < uid.length; i++) {
            long uidSegment = uid[uid.length - i - 1];
            long uidSegmentOther = other.uid[uid.length - i - 1];
            if (uidSegment > uidSegmentOther) {
                return true;
            }
            if (uidSegment < uidSegmentOther) {
                return false;
            }
        }
        return false;

    }
}
