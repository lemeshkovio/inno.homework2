package org.innopolis.lemeshkov.unicomparable;

public class UniComparator {

    /**
     * Compares two objects that implement UniComparable by comparing their tokens
     * @param first - first object
     * @param second - second object
     * @return - 1 if first larger, -1 if first less,
     *           0 if objects tokens are equal
     */
    public static int compare(UniComparable first, UniComparable second) {
        final UniComparableToken firstToken = first.getComparableToken();
        final UniComparableToken secondToken = second.getComparableToken();
        if (firstToken.greaterThan(secondToken)) {
            return 1;
        } else if (secondToken.greaterThan(firstToken)) {
            return -1;
        } else {
            return 0;
        }
    }
}
