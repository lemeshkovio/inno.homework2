package org.innopolis.lemeshkov.data_unit;

import java.util.Arrays;

import org.innopolis.lemeshkov.exceptions.KeyNotPresentException;

/**
 * Class to forms key-value pairs list
 */
public class Node {

    KeyValuePair pair;
    private Node next;
    private long height;

    /**
     * Create new Node with key-value pair
     *
     * @param key
     *            - new key
     * @param value
     *            - new value
     */
    public Node(Object key, Object value) {
        height = 1L;
        this.pair = new KeyValuePair(key, value);
    }

    /**
     * If key does not exist in this list, then
     * inserts a new node into this list containing the specified key-value
     * pair. If the given key is already in the list, then replaces the
     * corresponding value.
     *
     * @param key - key, contained in inserted node
     * @param value - value, contained in inserted node
     * @return - true, if new Node created, false else
     */
    public boolean putIntoList(Object key, Object value) {
        if (pair.hasKey(key)) {
            pair = new KeyValuePair(key, value);
            return false;
        }
        if (next != null) {
            boolean res = next.putIntoList(key, value);
            if (res) {
                height++;
            }
            return res;
        } else {
            next = new Node(key, value);
            height++;
            return true;
        }
    }

    /**
     * Remove from list node with current key
     *
     * @param key - current key to remove
     * @return - new head of list
     * @throws KeyNotPresentException - throw if key not exist
     */
    public Node removeFromList(Object key) throws KeyNotPresentException {
        if (pair.hasKey(key)) {
            height--;
            return this.next;
        } else {
            if (this.next != null) {
                this.next = this.next.removeFromList(key);
                height--;
                return this;
            } else {
                throw new KeyNotPresentException("Specified to be removed key do not exist on the list");
            }
        }
    }

    /**
     * Check of existing of current key
     *
     * @param key - current key
     * @return - true, if key exist, false if not
     */
    public boolean containsKey(Object key) {
        if (pair.hasKey(key)) {
            return true;
        }
        if (next != null) {
            return next.containsKey(key);
        } else {
            return false;
        }
    }

    /**
     * Check of existing key-value pair in list
     *
     * @param pair - current key-value pair
     * @return - true, if pair exist, false if not
     */
    public boolean containsPair(KeyValuePair pair) {
        if (this.pair.equals(pair)) {
            return true;
        }
        if (next != null) {
            return next.containsPair(pair);
        } else {
            return false;
        }
    }

    /**
     * Replace value with current key
     *
     * @param key - current key
     * @param value - new value
     * @throws KeyNotPresentException - throw if current key not exist
     */
    public void replaceValue(Object key, Object value) throws KeyNotPresentException {
        if (pair.hasKey(key)) {
            pair = new KeyValuePair(key, value);
            return;
        }
        if (next != null) {
            next.replaceValue(key, value);
        } else {
            throw new KeyNotPresentException("Specified for value replacement key do not exist on the list");
        }
    }

    /**
     * Returns current key value
     *
     * @param key  - current key
     * @throws KeyNotPresentException - throw if current key not exist
     */
    public Object getValue(Object key) throws KeyNotPresentException {
        if (pair.hasKey(key)) {
            return pair.getValue();
        }
        if (next != null) {
            return next.getValue(key);
        } else {
            throw new KeyNotPresentException("Specified to be retrieved key do not exist on the list");
        }
    }

    /**
     * Returns array of all key-value pair
     *
     * @return - array of all key-value pair
     */
    public KeyValuePair[] getKeyValuePairs() {
        if (next != null) {
            KeyValuePair[] nextPairs = next.getKeyValuePairs();
            KeyValuePair[] pairs = Arrays.copyOf(nextPairs, nextPairs.length + 1);
            pairs[pairs.length - 1] = pair;
            return pairs;
        } else {
            KeyValuePair[] pairs = new KeyValuePair[1];
            pairs[0] = pair;
            return pairs;
        }
    }

    /**
     * Returns array of hashCode of each key-value pair
     *
     * @return - array of hashCode of each key-value pair
     */
    public int[] getKeyValuePairsHashes() {
        final KeyValuePair[] pairs = getKeyValuePairs();
        int[] hashes = new int[pairs.length];
        for (int i = 0; i < pairs.length; i++) {
            hashes[i] = pairs[i].hashCode();
        }
        return hashes;
    }

    /**
     * Adds description for each pair in list
     *
     * @param line - description.
     */
    public void describeList(StringBuilder line) {
        pair.describeSelf(line);
        if (this.next != null) {
            line.append(",");
            this.next.describeList(line);
        }
    }

    /**
     * Checks if a given list is a subset of another
     *
     * @param node - Head of current list
     * @return - true, if all elements of current list exist in another
     */
    private boolean isSubListOf(Node node) {
        final KeyValuePair[] pairs = getKeyValuePairs();
        for (KeyValuePair pair : pairs) {
            if (!node.containsPair(pair)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder line = new StringBuilder();
        describeList(line);
        return "List{" + line + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final Node node = (Node) o;
        if (this.height != node.height) { 
            return false;
        }
        return isSubListOf(node); 
    }

    @Override
    public int hashCode() {
        int[] hashes = getKeyValuePairsHashes();
        Arrays.sort(hashes);
        return Arrays.hashCode(hashes);
    }
}
