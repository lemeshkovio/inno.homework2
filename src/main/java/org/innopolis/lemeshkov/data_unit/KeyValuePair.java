package org.innopolis.lemeshkov.data_unit;

import java.util.Objects;

/**
 * Immutable pair of key-value
 */
public class KeyValuePair {

    private final Object key;
    private final Object value;

    /**
     * Create new pair of key-value
     * 
     * @param key
     *            - new key
     * @param value
     *            - new value
     */
    public KeyValuePair(Object key, Object value) {
        this.key = key;
        this.value = value;
    }

    public Object getKey() {
        return key;
    }
    public Object getValue() {
        return value;
    }

    /**
     * Checks if the given key matches the key of this object
     * 
     * @param key - checking key
     * @return - true, if keys matches, false else
     */
    public boolean hasKey(Object key) {
        return Objects.equals(this.key, key);
    }

    /**
     * Forms StringBuilder line
     * 
     * @param outLine - StringBuilder line which contain key and value description
     */
    public void describeSelf(StringBuilder outLine) {
        outLine.append("{key=");
        outLine.append(key);
        outLine.append(", value=");
        outLine.append(value);
        outLine.append("}");
    }

    @Override
    public String toString() {
        final StringBuilder strB = new StringBuilder();
        describeSelf(strB);
        return "KeyValuePair" + strB;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KeyValuePair that = (KeyValuePair) o;
        return Objects.equals(key, that.key) && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }
}
